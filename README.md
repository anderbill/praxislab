# praxisLab

Workbench for static website experiments. I try to keep the lab clean.

[![Netlify Status](https://api.netlify.com/api/v1/badges/ce3edb56-0b42-42b2-ae6d-f53ca6706bd0/deploy-status)](https://app.netlify.com/sites/praxislab/deploys)
