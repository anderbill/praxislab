---
title: "Daily atmospheric carbon dioxide concentration, Earth."
hero_image: "paPelicans-201606.jpg"
date: 2020-12-31
---

Atmospheric carbon dioxide record from [Scripps Institute of Oceanography at UC San Diego.](https://keelingcurve.ucsd.edu/)

[![The lastest reading.](https://scripps.ucsd.edu/bluemoon/co2_400/daily_value.png)](https://scripps.ucsd.edu/bluemoon/co2_400/daily_value.png)
[![Carbon dioxide concentration at Mauna Loa Observatory](https://scripps.ucsd.edu/bluemoon/co2_400/mlo_full_record.png)](https://scripps.ucsd.edu/bluemoon/co2_400/mlo_full_record.png)


